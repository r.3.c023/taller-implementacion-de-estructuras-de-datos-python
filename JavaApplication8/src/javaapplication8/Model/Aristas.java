
package javaapplication8.Model;


public class Aristas {
      private final int destination;
    private final int weight;

    public Aristas(int destination, int weight) {
        this.destination = destination;
        this.weight = weight;
    }

    public int getDestino() {
        return destination;
    }

    public int getWeight() {
        return weight;
    }
    
}
