
package javaapplication8.Model;

import java.util.LinkedList;
import java.util.NoSuchElementException;


public class Pila {
    
    private LinkedList<Object> list = new LinkedList<>();

    public void push(Object value) {
        list.addFirst(value);
    }

    public Object pop() {
        if (list.isEmpty()) {
            throw new NoSuchElementException("Stack is empty");
        }
        return list.removeFirst();
    }

    public boolean isEmpty() {
        return list.isEmpty();
    }

    public LinkedList<Object> getList() {
        return list;
    } 
}
