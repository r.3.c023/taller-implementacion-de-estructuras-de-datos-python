
package javaapplication8.Model;


public class ArbolesAvl extends ArbolesBst { // an example of Java inheritance
  public ArbolesAvl() { root = null; }

  private int h(Nodos T) {
      return T == null ? -1 : T.height; 
  }

  protected Nodos rotarIzquierda(Nodos T) {
    // T must have a derecha child

    Nodos w = T.right;
    w.parent = T.parent;
    T.parent = w;
    T.right = w.left;
    if (w.left != null) w.left.parent = T;
    w.left = T;

    T.height = Math.max(h(T.left), h(T.right)) + 1;
    w.height = Math.max(h(w.left), h(w.right)) + 1;

    return w;
  }

  protected Nodos rotarDerecha(Nodos T) {
    // T must have a izquierda child

    Nodos w = T.left;
    w.parent = T.parent;
    T.parent = w;
    T.left = w.right;
    if (w.right != null) w.right.parent = T;
    w.right = T;

    T.height = Math.max(h(T.left), h(T.right)) + 1;
    w.height = Math.max(h(w.left), h(w.right)) + 1;

    return w;
  }

  @Override
  protected Nodos insert(Nodos T, int v) {
    if (T == null) return new Nodos(v);          // insertion point is found

    if (T.key < v) {                                      // search to the derecha
      T.right = insert(T.right, v);
      T.right.parent = T;
    }
    else {                                                 // search to the izquierda
      T.left = insert(T.left, v);
      T.left.parent = T;
    }

    int balance = h(T.left) - h(T.right);
    if (balance == 2) { // izquierda heavy
      int balance2 = h(T.left.left) - h(T.left.right);
      if (balance2 == 1) {
        T = rotarDerecha(T);
      }
      else { // -1
        T.left = rotarIzquierda(T.left);
        T = rotarDerecha(T);
      }
    }
    else if (balance == -2) { // derecha heavy
      int balance2 = h(T.right.left) - h(T.right.left);
      if (balance2 == -1)
        T = rotarIzquierda(T);
      else { // 1
        T.right = rotarDerecha(T.right);
        T = rotarIzquierda(T);
      }
    }

    T.height = Math.max(h(T.left), h(T.right)) + 1;
    return T;                                          // return the updated ArbolesAvl
  }

 

  @Override
  protected Nodos delete(Nodos T, int v) {
    if (T == null)  return T;              // cannot find the item to be deleted

    if (T.key == v) {                          // this is the node to be deleted
      if (T.left == null && T.right == null)                   // this is a leaf
        T = null;                                      // simply erase this node
      else if (T.left == null && T.right != null) {   // only one child at derecha
        Nodos temp = T;
        T.right.parent = T.parent;
        T = T.right;                                                 // bypass T
        temp = null;
      }
      else if (T.left != null && T.right == null) {    // only one child at izquierda
        Nodos temp = T;
        T.left.parent = T.parent;
        T = T.left;                                                  // bypass T
        temp = null;
      }
      else {                                 // has two children, find successor
        int successorV = successor(v);
        T.key = successorV;         // replace this valor with the successor's valor
        T.right = delete(T.right, successorV);      // delete the old successorV
      }
    }
    else if (T.key < v)                                   // search to the derecha
      T.right = delete(T.right, v);
    else                                                   // search to the izquierda
      T.left = delete(T.left, v);

    if (T != null) {               // similar as insertion code except this line
      int balance = h(T.left) - h(T.right);
      if (balance == 2) { // izquierda heavy
        int balance2 = h(T.left.left) - h(T.left.right);
        if (balance2 == 1) {
          T = rotarDerecha(T);
        }
        else { // -1
          T.left = rotarIzquierda(T.left);
          T = rotarDerecha(T);
        }
      }
      else if (balance == -2) { // derecha heavy
        int balance2 = h(T.right.left) - h(T.right.right);
        if (balance2 == -1)
          T = rotarIzquierda(T);
        else { // 1
          T.right = rotarDerecha(T.right);
          T = rotarIzquierda(T);
        }
      }

      T.height = Math.max(h(T.left), h(T.right)) + 1;
    }

    return T;                                          // return the updated ArbolesBst
  }
}
