
package javaapplication8.Model;

import java.util.LinkedList;
import java.util.NoSuchElementException;


public class Cola {
     private LinkedList<Object> list = new LinkedList<>();

    public void encolar(Object value) {
        list.addLast(value);
    }

    public Object desencolar() {
        if (list.isEmpty()) {
            throw new NoSuchElementException("Queue is empty");
        }
        return list.removeFirst();
    }

    public boolean isEmpty() {
        return list.isEmpty();
    }

    public LinkedList<Object> getList() {
        return list;
    }
}



