/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package javaapplication8.Model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author johan
 */
public class Grafo {
    private final Map<Integer, ArrayList<Aristas>> adjList;

    public Grafo() {
        adjList = new HashMap<>();
    }

    public void addVertice(int vertex) {
        adjList.putIfAbsent(vertex, new ArrayList<>());
    }

    public void addEdge(int source, int destination, int weight) {
        adjList.putIfAbsent(source, new ArrayList<>());
        adjList.putIfAbsent(destination, new ArrayList<>());
        adjList.get(source).add(new Aristas(destination, weight));
    }

    public Map<Integer, ArrayList<Aristas>> getAdjList() {
        return adjList;
    }
    
}
