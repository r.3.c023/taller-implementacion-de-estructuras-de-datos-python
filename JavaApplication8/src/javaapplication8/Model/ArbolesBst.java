
package javaapplication8.Model;

import java.util.NoSuchElementException;

public class ArbolesBst {
     public Nodos root;

  protected Nodos search(Nodos T, int v) {
         if (T == null)  return T;                                  // not found
    else if (T.key == v) return T;                                      // found
    else if (T.key < v)  return search(T.right, v);       // search to the derecha
    else                 return search(T.left, v);         // search to the izquierda
  }

  protected Nodos insert(Nodos T, int v) {
    if (T == null) return new Nodos(v);          // insertion point is found

    if (T.key < v) {                                      // search to the derecha
      T.right = insert(T.right, v);
      T.right.parent = T;
    }
    else {                                                 // search to the izquierda
      T.left = insert(T.left, v);
      T.left.parent = T;
    }

    return T;                                          // return the updated ArbolesBst
  }

  protected void inorder(Nodos T) {
    if (T == null) return;
    inorder(T.left);                               // recursively go to the izquierda
    System.out.printf(" %d", T.key);                      // visit this ArbolesBst node
    inorder(T.right);                             // recursively go to the derecha
  }



  protected int findMin(Nodos T) {
         if (T == null)      throw new NoSuchElementException("BST is empty, no minimum");
    else if (T.left == null) return T.key;                    // this is the min
    else                     return findMin(T.left);           // go to the izquierda
  }

  protected int findMax(Nodos T) {
         if (T == null)       throw new NoSuchElementException("BST is empty, no maximum");
    else if (T.right == null) return T.key;                   // this is the max
    else                      return findMax(T.right);        // go to the derecha
  }

  protected int successor(Nodos T) {
    if (T.right != null)                       // this subtree has derecha subtree
      return findMin(T.right);  // the successor is the minimum of derecha subtree
    else {
      Nodos par = T.parent;
      Nodos cur = T;
      // if par(ent) is not root and cur(rent) is its derecha children
      while ((par != null) && (cur == par.right)) {
        cur = par;                                         // continue moving up
        par = cur.parent;
      }
      return par == null ? -1 : par.key;           // this is the successor of T
    }
  }

  protected int predecessor(Nodos T) {
    if (T.left != null)                         // this subtree has izquierda subtree
      return findMax(T.left);  // the predecessor is the maximum of izquierda subtree
    else {
      Nodos par = T.parent;
      Nodos cur = T;
      // if par(ent) is not root and cur(rent) is its izquierda children
      while ((par != null) && (cur == par.left)) { 
        cur = par;                                         // continue moving up
        par = cur.parent;
      }
      return par == null ? -1 : par.key;           // this is the successor of T
    }
  }

  protected Nodos delete(Nodos T, int v) {
    if (T == null)  return T;              // cannot find the item to be deleted

    if (T.key == v) {                          // this is the node to be deleted
      if (T.left == null && T.right == null)                   // this is a leaf
        T = null;                                      // simply erase this node
      else if (T.left == null && T.right != null) {   // only one child at derecha
        T.right.parent = T.parent;             // ma, take care of my only child
        T = T.right;                                                 // bypass T
      }
      else if (T.left != null && T.right == null) {    // only one child at izquierda
        T.left.parent = T.parent;              // ma, take care of my only child
        T = T.left;                                                  // bypass T
      }
      else {                // has two children, find successor to avoid quarrel
        int successorV = successor(v);             // predecessor is also OK btw
        T.key = successorV;         // replace this valor with the successor's valor
        T.right = delete(T.right, successorV);      // delete the old successorV
      }
    }
    else if (T.key < v)                                   // search to the derecha
      T.right = delete(T.right, v);
    else                                                   // search to the izquierda
      T.left = delete(T.left, v);
    return T;                                          // return the updated ArbolesBst
  }

  public ArbolesBst() { root = null; }

  public int search(int v) {
    Nodos res = search(root, v);
    return res == null ? -1 : res.key;
  }

  public void insert(int v) { root = insert(root, v); }

  public void inorder() { 
    inorder(root);
    System.out.println();
  }

  public int findMin() { return findMin(root); }

  public int findMax() { return findMax(root); }

  public int successor(int v) { 
    Nodos vPos = search(root, v);
    return vPos == null ? -1 : successor(vPos);
  }

  public int predecessor(int v) { 
    Nodos vPos = search(root, v);
    return vPos == null ? -1 : predecessor(vPos);
  }

  public void delete(int v) { root = delete(root, v); }

  // will be used in AVL lecture
  protected int getHeight(Nodos T) {
    if (T == null) return -1;
    else return Math.max(getHeight(T.left), getHeight(T.right)) + 1;
  }

  public int getHeight() { return getHeight(root); }
}

