package javaapplication8.controller;

import java.util.*;
import javaapplication8.Model.*;
import javaapplication8.view.EstructuraDatosVista;

public class EstrucutraDatosControl {
    private ArbolesBst bst;
    private ArbolesAvl avl;
    private Cola cola;
    private Pila pila;
    private Grafo grafo;
    private EstructuraDatosVista vista;

    public EstrucutraDatosControl() {
        bst = new ArbolesBst();
        avl = new ArbolesAvl();
        cola = new Cola();
        pila = new Pila();
        grafo = new Grafo();
        vista = new EstructuraDatosVista(bst, avl, cola, pila, grafo);
    }

    public void Insertar(int element) throws InterruptedException {
        bst.insert(element);
        avl.insert(element);
        vista.updatePanels(bst, avl, cola, pila, grafo);
        vista.highlightElement(element);
        Thread.sleep(1000);
    }

    public void buscar(int element) throws InterruptedException {
        bst.search(element);
        avl.search(element);
        vista.updatePanels(bst, avl, cola, pila, grafo);
        vista.highlightElement(element);
        Thread.sleep(1000);
    }

    public void eliminar(int element) throws InterruptedException {
        vista.highlightElement(element);
        Thread.sleep(1000);
        bst.delete(element);
        avl.delete(element);
        vista.updatePanels(bst, avl, cola, pila, grafo);
    }

    public void encolar(int element) throws InterruptedException {
        cola.encolar(element);
        vista.highlightElement(element);
        vista.updatePanels(bst, avl, cola, pila, grafo);
        Thread.sleep(1000);
    }

    public void apilar(int elemento) throws InterruptedException {
        pila.push(elemento);
        vista.highlightElement(elemento);
        vista.updatePanels(bst, avl, cola, pila, grafo);
        Thread.sleep(1000);
    }

    public int tamanoCola() {
        return cola.getList().size();
    }

    public int tamanoPila() {
        return pila.getList().size();
    }

    public void desencolar() {
        cola.desencolar();
        vista.updatePanels(bst, avl, cola, pila, grafo);
    }

    public void desapilar() {
        pila.pop();
        vista.updatePanels(bst, avl, cola, pila, grafo);
    }

    // BFS en ArbolesBst
    public ArrayList<Integer> bfsBST() {
        return bfs(bst.root);
    }

    // BFS en ArbolesAvl
    public ArrayList<Integer> bfsAVL() {
        return bfs(avl.root);
    }

    private ArrayList<Integer> bfs(Nodos root) {
        ArrayList<Integer> result = new ArrayList<>();
        if (root == null) {
            return result;
        }

        Queue<Nodos> cola = new LinkedList<>();
        cola.add(root);

        while (!cola.isEmpty()) {
            Nodos node = cola.poll();
            result.add(node.key);

            if (node.left != null) {
                cola.add(node.left);
            }
            if (node.right != null) {
                cola.add(node.right);
            }
        }

        return result;
    }

    // DFS en ArbolesBst
    public ArrayList<Integer> dfsBST() {
        return dfs(bst.root);
    }

    // DFS en ArbolesAvl
    public ArrayList<Integer> dfsAVL() {
        return dfs(avl.root);
    }

    private ArrayList<Integer> dfs(Nodos root) {
        ArrayList<Integer> result = new ArrayList<>();
        if (root == null) {
            return result;
        }
        Stack<Nodos> pila = new Stack<>();
        pila.push(root);

        while (!pila.isEmpty()) {
            Nodos node = pila.pop();
            result.add(node.key);

            if (node.right != null) {
                pila.push(node.right);
            }
            if (node.left != null) {
                pila.push(node.left);
            }
        }

        return result;
    }

    public void mostrarBFSBST() {
        ArrayList<Integer> bfsResult = bfsBST();
        vista.updateBFSPanel(bfsResult);
    }

    public void mostrarBFSAVL() {
        ArrayList<Integer> bfsResult = bfsAVL();
        vista.updateBFSPanel(bfsResult);
    }

    public void mostrarDFSBST() {
        ArrayList<Integer> dfsResult = dfsBST();
        vista.updateDFSPanel(dfsResult);
    }

    public void mostrarDFSAVL() {
        ArrayList<Integer> dfsResult = dfsAVL();
        vista.updateDFSPanel(dfsResult);
    }
    
   
    public Grafo getGraph() {
         vista.updatePanels(bst, avl, cola, pila, grafo);
        return grafo;
    }

  public Map<Integer, Integer> dijkstra(int source) {
    Map<Integer, Integer> distances = new HashMap<>();
    PriorityQueue<int[]> pq = new PriorityQueue<>(Comparator.comparingInt(a -> a[1]));

    for (int vertex : grafo.getAdjList().keySet()) {
        distances.put(vertex, Integer.MAX_VALUE);
    }
    distances.put(source, 0);
    pq.add(new int[]{source, 0});

    while (!pq.isEmpty()) {
        int[] current = pq.poll();
        int currentVertex = current[0];
        int currentDistance = current[1];

        if (currentDistance > distances.get(currentVertex)) {
            continue;
        }

        for (Aristas edge : grafo.getAdjList().get(currentVertex)) {
            int newDist = distances.get(currentVertex) + edge.getWeight();
            if (newDist < distances.get(edge.getDestino())) {
                distances.put(edge.getDestino(), newDist);
                pq.add(new int[]{edge.getDestino(), newDist});
            }
        }
       
    }
    return distances;
}

}

