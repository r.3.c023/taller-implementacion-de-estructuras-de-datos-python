
package javaapplication8.view;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;

public class BFSVista extends JPanel {

    private ArrayList<Integer> bfsResult;
    private Integer highlightedValue = null;

    public BFSVista() {
        this.bfsResult = new ArrayList<>();
    }

    public void setBFSResult(ArrayList<Integer> bfsResult) {
        this.bfsResult = bfsResult;
        
        repaint();
    }

    public void highlightValue(Integer value) {
        this.highlightedValue = value;
        repaint();
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        int x = 20;
        int y = 30;
        for (Integer value : bfsResult) {
            if (highlightedValue != null && value.equals(highlightedValue)) {
                g.setColor(Color.RED);
            } else {
                g.setColor(Color.BLACK);
            }
            g.drawRect(x, y, 50, 30);
            g.drawString(Integer.toString(value), x + 20, y + 20);
            x += 60;
        }
    }
}
