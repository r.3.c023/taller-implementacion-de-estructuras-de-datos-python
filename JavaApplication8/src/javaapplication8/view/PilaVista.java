
package javaapplication8.view;

import javaapplication8.Model.Pila;
import java.awt.Color;
import java.awt.Graphics;
import java.util.LinkedList;
import javax.swing.JPanel;


public class PilaVista extends JPanel {
   private Pila stack;
    private Integer highlightedValue = null;

    public PilaVista(Pila stack) {
        this.stack = stack;
    }

    public void setStack(Pila stack) {
        this.stack = stack;
        repaint();
    }

    public void highlightValue(Integer value) {
        this.highlightedValue = value;
        repaint();
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        LinkedList<Object> list = stack.getList();
        int x = 20;
        int y = 30;
        for (Object value : list) {
            if (highlightedValue != null && value == highlightedValue) {
                g.setColor(Color.RED);
            } else {
                g.setColor(Color.BLACK);
            }
            g.drawRect(x, y, 50, 30);
            g.drawString(Integer.toString((int) value), x + 20, y + 20);
            y += 40;
        }
    }
}
