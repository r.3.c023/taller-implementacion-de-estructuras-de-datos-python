
package javaapplication8.view;

import javaapplication8.Model.Cola;
import java.awt.Color;
import java.awt.Graphics;
import java.util.Iterator;
import java.util.LinkedList;
import javax.swing.JPanel;


public class ColaVista extends JPanel {
   
    private Cola queue;
    private Integer highlightedValue = null;

    public ColaVista(Cola queue) {
        this.queue = queue;
    }

    public void setQueue(Cola queue) {
        this.queue = queue;
        repaint();
    }

    public void highlightValue(Integer value) {
        this.highlightedValue = value;
        repaint();
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        LinkedList<Object> list = queue.getList();
        int x = 20;
        int y = 30;
        for (Iterator<Object> it = list.iterator(); it.hasNext();) {
            int value = (int) it.next();
            if (highlightedValue != null && value == highlightedValue) {
                g.setColor(Color.RED);
            } else {
                g.setColor(Color.BLACK);
            }
            g.drawRect(x, y, 50, 30);
            g.drawString(Integer.toString(value), x + 20, y + 20);
            x += 60;
        }
    }
}




