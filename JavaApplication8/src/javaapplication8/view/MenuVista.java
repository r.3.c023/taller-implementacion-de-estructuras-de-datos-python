package javaapplication8.view;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.Map;
import javaapplication8.controller.EstrucutraDatosControl;

public class MenuVista extends JFrame {
    private byte opcion;
    EstrucutraDatosControl controller = new EstrucutraDatosControl();
    
    private JPanel menuPanel;

    public MenuVista() {
        setTitle("Menu Frame");
        setSize(400, 200);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLocationRelativeTo(null);
        setLayout(new BorderLayout());

        menuPanel = new JPanel(new CardLayout());

        // Create and add the main menu panel
        menuPanel.add(createMainMenuPanel(), "Main Menu");
        // Create and add submenus dynamically when needed
        for (int i = 0; i < 8; i++) {  // Updated to 8 for new Dijkstra menu
            menuPanel.add(createSubMenuPanel(i), "SubMenu" + i);
        }

        add(menuPanel, BorderLayout.CENTER);

        JButton menuButton = new JButton("Show Menu");
        menuButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                showCard("Main Menu");
            }
        });

        add(menuButton, BorderLayout.SOUTH);
    }

    private JPanel createMainMenuPanel() {
        JPanel panel = new JPanel();
        panel.setLayout(new GridLayout(3, 3));

        JButton[] buttons = new JButton[9];
        String[] buttonLabels = {"Arboles AVL", "Arboles BST", "Arboles B Plus", "Colas", "Pilas", "BFS", "DFS", "Dijkstra", "Salir"};

        for (int i = 0; i < 9; i++) {
            buttons[i] = new JButton(buttonLabels[i]);
            int choice = i; // Final copy for use in the action listener
            buttons[i].addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    if (choice != 8) {
                        opcion = (byte) (choice + 1);  // Opciones desde 1 a 8
                        showCard("SubMenu" + choice);
                    } else {
                        System.exit(0);
                    }
                }
            });
            panel.add(buttons[i]);
        }

        return panel;
    }

    private JPanel createSubMenuPanel(int menuIndex) {
        JPanel panel = new JPanel();
        panel.setLayout(new GridLayout(4, 1));

        JButton[] buttons = new JButton[4];
        String[] buttonLabels;
        if (menuIndex == 3) { // Colas
            buttonLabels = new String[]{"Encolar", "Desencolar", "Tamaño de la Cola", "Volver"};
        } else if (menuIndex == 4) { // Pilas
            buttonLabels = new String[]{"Apilar", "Desapilar", "Tamaño de la Pila", "Volver"};
        } else if (menuIndex == 5 || menuIndex == 6) { // BFS y DFS
            
            buttonLabels = new String[]{"Mostrar en BST", "Mostrar en AVL", "Volver"};
        } else if (menuIndex == 7) { // Dijkstra
            buttonLabels = new String[]{"Agregar Vértice", "Agregar Arista", "Ejecutar Dijkstra", "Volver"};
        } else {
            buttonLabels = new String[]{"Inserción", "Búsqueda", "Eliminar", "Volver"};
        }

        for (int i = 0; i < buttonLabels.length; i++) {
            buttons[i] = new JButton(buttonLabels[i]);
            int choice = i; // Final copy for use in the action listener
            buttons[i].addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    if (choice != buttonLabels.length - 1) {
                        try {
                            mostrarSubmenus((byte) (choice + 1), menuIndex);
                        } catch (InterruptedException ex) {
                            Logger.getLogger(MenuVista.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    } else {
                        showCard("Main Menu");
                    }
                }
            });
            panel.add(buttons[i]);
        }

        return panel;
    }

    private void showCard(String cardName) {
        CardLayout cl = (CardLayout) (menuPanel.getLayout());
        cl.show(menuPanel, cardName);
    }

    public void mostrarSubmenus(byte opcion, int menuIndex) throws InterruptedException {
        int elemento;
        String nombre;
        switch (menuIndex) {
            case 0: nombre = "AVL"; break;
            case 1: nombre = "BST"; break;
            case 2: nombre = "B Plus"; break;
            case 3: nombre = "Colas"; break;
            case 4: nombre = "Pilas"; break;
            case 5: nombre = "BFS"; break;
            case 6: nombre = "DFS"; break;
            case 7: nombre = "Dijkstra"; break;
            default: throw new AssertionError();
        }

        switch (opcion) {
            case 1:
                if (menuIndex == 3) { // Colas - Encolar
                    elemento = Integer.parseInt(JOptionPane.showInputDialog("Ingrese el valor a encolar"));
                    controller.encolar(elemento);
                } else if (menuIndex == 4) { // Pilas - Apilar
                    elemento = Integer.parseInt(JOptionPane.showInputDialog("Ingrese el valor a apilar"));
                    controller.apilar(elemento);
                    
                } else if (menuIndex == 7) { // Dijkstra - Agregar Vértice
                    elemento = Integer.parseInt(JOptionPane.showInputDialog("Ingrese el valor del vértice"));
                    controller.getGraph().addVertice(elemento);
                } else { // General - Insertar
                    elemento = Integer.parseInt(JOptionPane.showInputDialog("Ingrese el valor a insertar"));
                    controller.Insertar(elemento);
                }
                break;
            case 2:
                if (menuIndex == 3) { // Colas - Desencolar
                    controller.desencolar();
                } else if (menuIndex == 4) { // Pilas - Desapilar
                    controller.desapilar();
                } else if (menuIndex == 7) { // Dijkstra - Agregar Arista
                    int source = Integer.parseInt(JOptionPane.showInputDialog("Ingrese el vértice origen"));
                    int destination = Integer.parseInt(JOptionPane.showInputDialog("Ingrese el vértice destino"));
                    int weight = Integer.parseInt(JOptionPane.showInputDialog("Ingrese el peso de la arista"));
                    controller.getGraph().addEdge(source, destination, weight);
                } else { // General - Buscar
                    elemento = Integer.parseInt(JOptionPane.showInputDialog("Ingrese el valor a buscar"));
                    controller.buscar(elemento);
                }
                break;
            case 3:
                if (menuIndex == 3) { // Colas - Tamaño
                    int size = controller.tamanoCola();
                    JOptionPane.showMessageDialog(this, "Tamaño de la Cola: " + size);
                } else if (menuIndex == 4) { // Pilas - Tamaño
                    int size = controller.tamanoPila();
                    JOptionPane.showMessageDialog(this, "Tamaño de la Pila: " + size);
                } else if (menuIndex == 7) { // Dijkstra - Ejecutar Dijkstra
                    int source = Integer.parseInt(JOptionPane.showInputDialog("Ingrese el vértice de inicio"));
                    Map<Integer, Integer> distances = controller.dijkstra(source);
                    StringBuilder result = new StringBuilder("Distancias desde el vértice " + source + ":\n");
                    for (Map.Entry<Integer, Integer> entry : distances.entrySet()) {
                        result.append("Vértice ").append(entry.getKey()).append(": ").append(entry.getValue()).append("\n");
                    }
                    JOptionPane.showMessageDialog(this, result.toString());
                } else { // General - Eliminar
                    elemento = Integer.parseInt(JOptionPane.showInputDialog("Ingrese el valor a eliminar"));
                    controller.eliminar(elemento);
                }
                case 4:if (menuIndex == 5) {
               
                    controller.bfsBST();
                }
                case 5: 
                    if (menuIndex == 6) {
                    
                    
                    }
                        break;
                
            default:
                throw new AssertionError();
        }

       
    }

   
}
