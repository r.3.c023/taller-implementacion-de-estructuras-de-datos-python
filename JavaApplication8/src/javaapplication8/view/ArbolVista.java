
package javaapplication8.view;
import javaapplication8.Model.Nodos;
    import javax.swing.*;
import java.awt.*;

public class ArbolVista extends JPanel {

    private Nodos root;
    private Integer highlightedKey = null;

    public ArbolVista(Nodos root) {
        this.root = root;
    }

    public void setRoot(Nodos root) {
        this.root = root;
        repaint();
    }

    public void highlightKey(Integer key) {
        this.highlightedKey = key;
        repaint();
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        if (root != null) {
            drawTree(g, root, getWidth() / 2, 30, getWidth() / 4);
        }
    }

    private void drawTree(Graphics g, Nodos node, int x, int y, int xOffset) {
        if (node == null) return;
        if (node.left != null) {
            g.drawLine(x, y, x - xOffset, y + 50);
            drawTree(g, node.left, x - xOffset, y + 50, xOffset / 2);
        }
        if (node.right != null) {
            g.drawLine(x, y, x + xOffset, y + 50);
            drawTree(g, node.right, x + xOffset, y + 50, xOffset / 2);
        }

        if (highlightedKey != null && node.key == highlightedKey) {
            g.setColor(Color.RED);
        } else {
            g.setColor(Color.BLACK);
        }
        g.fillOval(x - 15, y - 15, 30, 30);
        g.setColor(Color.WHITE);
        g.drawString(Integer.toString(node.key), x - 10, y + 5);
    }
    
}




