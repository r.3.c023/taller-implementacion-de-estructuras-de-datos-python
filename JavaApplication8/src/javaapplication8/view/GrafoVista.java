package javaapplication8.view;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import javaapplication8.Model.Grafo;
import javaapplication8.Model.Aristas;

public class GrafoVista extends JPanel {
    private Grafo graph;
    private Integer highlightedVertex = null;
    private Map<Integer, Integer> highlightedEdges = new HashMap<>();

    public GrafoVista(Grafo graph) {
        this.graph = graph;
    }

    public void setGraph(Grafo graph) {
        this.graph = graph;
        this.repaint();
    }

    public void highlightVertex(Integer vertex) {
        this.highlightedVertex = vertex;
        repaint();
    }

    public void highlightEdge(Integer source, Integer destination) {
        this.highlightedEdges.put(source, destination);
        repaint();
    }

    public void updateGraph(Grafo newGraph) {
        this.graph = newGraph;
        this.highlightedVertex = null;
        this.highlightedEdges.clear();
        this.repaint();
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        if (this.graph == null) {
            return;
        }

        Map<Integer, ArrayList<Aristas>> adjList = graph.getAdjList();
        Map<Integer, Point> vertexPositions = calculateVertexPositions(adjList.keySet());

        // Dibujar los vértices
        for (Map.Entry<Integer, Point> entry : vertexPositions.entrySet()) {
            int vertex = entry.getKey();
            Point position = entry.getValue();

            if (highlightedVertex != null && vertex == highlightedVertex) {
                g.setColor(Color.RED);
            } else {
                g.setColor(Color.BLACK);
            }

            g.fillOval(position.x - 15, position.y - 15, 30, 30);
            g.setColor(Color.WHITE);
            g.drawString(String.valueOf(vertex), position.x - 5, position.y + 5);
        }

        // Dibujar las aristas
        for (Map.Entry<Integer, ArrayList<Aristas>> entry : adjList.entrySet()) {
            int source = entry.getKey();
            Point sourcePosition = vertexPositions.get(source);
            List<Aristas> edges = entry.getValue();

            for (Aristas edge : edges) {
                int destination = edge.getDestino();
                Point destinationPosition = vertexPositions.get(destination);

                if (highlightedEdges.containsKey(source) && highlightedEdges.get(source) == destination) {
                    g.setColor(Color.RED);
                } else {
                    g.setColor(Color.BLACK);
                }

                g.drawLine(sourcePosition.x, sourcePosition.y, destinationPosition.x, destinationPosition.y);
                g.drawString(String.valueOf(edge.getWeight()),
                        (sourcePosition.x + destinationPosition.x) / 2,
                        (sourcePosition.y + destinationPosition.y) / 2);
            }
        }
    }

    private Map<Integer, Point> calculateVertexPositions(Iterable<Integer> vertices) {
        Map<Integer, Point> positions = new HashMap<>();
        int width = getWidth();
        int height = getHeight();
        int n = 0;

        for (Integer vertex : vertices) {
            n++;
        }

        int radius = Math.min(width, height) / 2 - 50;
        int centerX = width / 2;
        int centerY = height / 2;
        double angleIncrement = 2 * Math.PI / n;
        double angle = 0;

        for (Integer vertex : vertices) {
            int x = (int) (centerX + radius * Math.cos(angle));
            int y = (int) (centerY + radius * Math.sin(angle));
            positions.put(vertex, new Point(x, y));
            angle += angleIncrement;
        }

        return positions;
    }
}
