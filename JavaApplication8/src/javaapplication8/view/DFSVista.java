
package javaapplication8.view;

import java.awt.Color;
import java.awt.Graphics;
import java.util.ArrayList;
import javax.swing.JPanel;


public class DFSVista extends JPanel {
     private ArrayList<Integer> dfsResult;
    private Integer highlightedValue = null;

    public DFSVista() {
        this.dfsResult = new ArrayList<>();
    }

    public void setDFSResult(ArrayList<Integer> dfsResult) {
        this.dfsResult = dfsResult;
        repaint();
    }

    public void highlightValue(Integer value) {
        this.highlightedValue = value;
        repaint();
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        int x = 20;
        int y = 30;
        for (Integer value : dfsResult) {
            if (highlightedValue != null && value.equals(highlightedValue)) {
                g.setColor(Color.RED);
            } else {
                g.setColor(Color.BLACK);
            }
            g.drawRect(x, y, 50, 30);
            g.drawString(Integer.toString(value), x + 20, y + 20);
            x += 60;
        }
    }
}
