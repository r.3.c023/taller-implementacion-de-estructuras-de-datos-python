
package javaapplication8.view;

import java.util.ArrayList;
import javax.swing.*;
import javaapplication8.Model.*;

public class EstructuraDatosVista {
    private JFrame frame;
    private JTabbedPane tabbedPane;
    private ArbolVista bstPanel;
    private ArbolVista avlPanel;
    private ColaVista queuePanel;
    private PilaVista stackPanel;
    private MenuVista menuFrame;
    private BFSVista bfsPanel;
    private DFSVista dfsPanel;
    private GrafoVista graphPanel;
    
    

    public EstructuraDatosVista(ArbolesBst bst, ArbolesAvl avl, Cola queue, Pila stack, Grafo graph) {
        frame = new JFrame("Data Structure Visualizer");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(800, 600);
        
        
        bstPanel = new ArbolVista(bst.root);
        avlPanel = new ArbolVista(avl.root);
        queuePanel = new ColaVista(queue);
        stackPanel = new PilaVista(stack);
        bfsPanel = new BFSVista();
        dfsPanel = new DFSVista();
        tabbedPane = new JTabbedPane();
        graphPanel = new GrafoVista(graph);
        
        tabbedPane.add("BST", bstPanel);
        tabbedPane.add("AVL", avlPanel);
        tabbedPane.add("Queue", queuePanel);
        tabbedPane.add("Stack", stackPanel);
        tabbedPane.add("BFS", bfsPanel);
        tabbedPane.add("DFS", dfsPanel);
        tabbedPane.add("Graph", graphPanel);

        frame.add(tabbedPane);
        frame.setVisible(true);
    }

    public void updatePanels(ArbolesBst bst, ArbolesAvl avl, Cola queue, Pila stack,  Grafo graph) {
        bstPanel.setRoot(bst.root);
        avlPanel.setRoot(avl.root);
        queuePanel.setQueue(queue);
        stackPanel.setStack(stack);
        graphPanel.setGraph(graph);
    }

    public void highlightElement(Integer element) {
        bstPanel.highlightKey(element);
        avlPanel.highlightKey(element);
        queuePanel.highlightValue(element);
        stackPanel.highlightValue(element);
        bfsPanel.highlightValue(element);
        dfsPanel.highlightValue(element);
        graphPanel.highlightVertex(element);
    }
    public void highlightEdge(Integer source, Integer destination) {
        graphPanel.highlightEdge(source, destination);
        
    }
      public void updateBFSPanel(ArrayList<Integer> bfsResult) {
        bfsPanel.setBFSResult(bfsResult);
    }

    public void updateDFSPanel(ArrayList<Integer> dfsResult) {
        dfsPanel.setDFSResult(dfsResult);
    }
  
    
}
